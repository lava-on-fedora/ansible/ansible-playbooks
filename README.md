# Lava Ansible Playbooks

A collection of ansible playbooks to deploy Lava.

Roles are stored in their own repository and referenced in `requirements.yml`.

## Usage

Install dependencies by running:

```sh
ansible-galaxy install -r requirements.yml
```

Copy `inventories/sample.ini` to someplace else, such as `inventories/staging.ini`,
set the vars to their correct/desired value and run one of the playbooks from 
the `playbooks` folder:

```sh
ansible-playbook -i inventories/staging.ini playbooks/deploy-worker.yml
```

## License

[MIT](./LICENSE)
